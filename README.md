<!--
Copyright 2023 Pipin Fitriadi <pipinfitriadi@gmail.com>

Licensed under the Microsoft Reference Source License (MS-RSL)

This license governs use of the accompanying software. If you use the
software, you accept this license. If you do not accept the license, do not
use the software.

1. Definitions

The terms "reproduce," "reproduction" and "distribution" have the same
meaning here as under U.S. copyright law.

"You" means the licensee of the software.

"Your company" means the company you worked for when you downloaded the
software.

"Reference use" means use of the software within your company as a reference,
in read only form, for the sole purposes of debugging your products,
maintaining your products, or enhancing the interoperability of your
products with the software, and specifically excludes the right to
distribute the software outside of your company.

"Licensed patents" means any Licensor patent claims which read directly on
the software as distributed by the Licensor under this license.

2. Grant of Rights

(A) Copyright Grant- Subject to the terms of this license, the Licensor
grants you a non-transferable, non-exclusive, worldwide, royalty-free
copyright license to reproduce the software for reference use.

(B) Patent Grant- Subject to the terms of this license, the Licensor grants
you a non-transferable, non-exclusive, worldwide, royalty-free patent
license under licensed patents for reference use.

3. Limitations

(A) No Trademark License- This license does not grant you any rights to use
the Licensor's name, logo, or trademarks.

(B) If you begin patent litigation against the Licensor over patents that
you think may apply to the software (including a cross-claim or counterclaim
in a lawsuit), your license to the software ends automatically.

(C) The software is licensed "as-is." You bear the risk of using it. The
Licensor gives no express warranties, guarantees or conditions. You may have
additional consumer rights under your local laws which this license cannot
change. To the extent permitted under your local laws, the Licensor excludes
the implied warranties of merchantability, fitness for a particular purpose
and non-infringement.
-->

# VPN

## Cara Pemakaian

1. Dapatkan kredensial Open VPN Jabar Cloud v2 dari DevOps JDS. Rincian langkah terkait kredensial sebagai berikut:
    - Simpan _file_ `*.ovpn` nya di _folder_ `{root_repo}/ovpn/*.ovpn`, lalu tambahkan baris baru berikut ini sebelum bagian _private key_ pada _file_-nya.

        ```ovpn
        askpass /vpn/.pass
        ```

    - Buat dan masukkan _password_ Open VPN ke dalam `{root_repo}/ovpn/.pass`.

2. Pastikan [Docker Desktop](https://www.docker.com/products/docker-desktop/) sudah berjalan pada komputer dan anda berada pada lokasi _folder_ `{root_repo}/`.
    - _Pull image_ docker:

        ```sh
        docker-compose pull
        ```

    - Menjalankan VPN:

        ```sh
        docker-compose up -d
        ```

    - Masuk ke _container_ docker:

        ```sh
        docker-compose exec vpn sh
        ```

    - Koneksi SSH:

        ```sh
        docker-compose exec vpn ssh $remote_username@$remote_host
        ```

    - _Port Forwarding_:

        ```sh
        docker-compose exec vpn socat tcp-listen:1989,fork,reuseaddr tcp-connect:$remote_host:$remote_port
        ```

        _Server remote_ sudah terkenoksi dengan komputer lokal anda. Anda dapat coba melakukan koneksi di komputer lokal ke `0.0.0.0:1989`.

        > Secara bersamaan tekan tombol `[Control]` + `[c]` di MacOS atau `[Ctrl]` + `[c]` di non MacOS, untuk mengakhiri _port forwarding_.

    - Mengakses _database_ menggunakan Adminer:

        ```sh
        docker-compose exec vpn run-adminer
        ```

        _Server Adminer_ dapat diakses di `localhost:8888`.

    - _PostgreSQL Clients_:

        ```sh
        docker-compose -f db.yaml run --rm psql -h $remote_database_host -p $remote_database_port -d $database_name -U $database_user
        ```

    - Ansible:

        ```sh
        docker-compose exec vpn ansible --help 
        ```

    - Mematikan VPN:

        ```sh
        docker-compose down -v
        ```

## Lisensi

Lisensi yang dipergunakan adalah [MS-RSL](LICENSE) (Microsoft Reference Source License).
